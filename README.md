# jwt-auth

This is a login micro service as part of a personal 
web page authentication process. It's job is to
provide a jwt to the user.

# todo

- [x] express server.
- [x] hashing on password.
- [x] wire to mongodb.
- [x] add eslint
- [x] replace config.json with .env
- [ ] test route for protected source.
- [ ] add Dockerfile and docker-compose.yml

## Installation

```bash
npm install
```

There are two ways of importing environment variables. 
The first is to set them manually when starting the app 
or using and `.env` file with the following variables:

```
PORT
BASE_URL
SECRET
HASHKEY
```

## Usage

Start the application with:

```bash
npm start
```

## API

The vertsion api:

| method | endpoint | response                                           |
|--------|----------|----------------------------------------------------|
| GET    | `/api`   | {  "service": `<string>`,  "version": `<string>` } |

Article api:

Root url: `/login`.

| method | endpoint          | description        |
|--------|-------------------|--------------------|
| POST   | `/authentication` | user autentication |

# test

Sign Up

```
curl -s -XPOST -H "content-type: application/json" -d '{"email": "espinoza.jimenez@gmail.com", "password": "hello world"}' localhost:4000/login/api/v1/users/  | jq .
```

Login

```console
curl -s -XPOST -H "content-type: application/json" -d '{ "email": "test@test.com", "password": "test"}' localhost:4000/login/api/v1/users/login/  | jq .
```

protected route `/me`

```
curl -s -H "content-type: application/json" -H "Authorization: $token" localhost:4000/login/api/v1/users/me/  | jq .
```

Logout

```console
curl -s -XPOST -H "content-type: application/json" -H "Authorization: $token" localhost:4000/login/api/v1/users/me/logout/  | jq .
```

Logoutall

```console
curl -s -XPOST -H "content-type: application/json" -H "Authorization: $token" localhost:4000/login/api/v1/users/me/logoutall/  | jq .
```

## Deployment

Remember to set the environmental variables listed in the Installation section.

```console
npm run start:prod
```

## Contributors

- [[sespinoz]](https://gitlab.com/sespinoz) Samuel Espinoza - creator, maintainer

## Acknowledgments

- [[fatukunda]](https://github.com/fatukunda)  Frank Atukunda - base code for the [user-registration-api](https://github.com/fatukunda/user-registration-api) Used with the author's permission.
