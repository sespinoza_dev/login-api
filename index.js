require('console-stamp')(console, '[HH:MM:ss.l]')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

if (process.env.NODE_ENV && process.env.NODE_ENV !== 'production') {
  console.log('loading env variables')
  dotenv.config()
}

const { startExpress } = require('./src/server')
const { name, version } = require('./package.json')

if (!process.env.PORT) console.warn('PORT env variable missing, default: 4000')
if (!process.env.BASE_URL) console.warn('BASE_URL env variable missing, default: localhost')
if (!process.env.SECRET) console.error('SECRET env variable missing, default: cupcake')
if (!process.env.HASHKEY) console.error('HASHKEY env variable missing, default: cupcake')

console.log(`Service: ${name}, version: ${version}`)
console.log(`NODE_ENV: ${process.env.NODE_ENV}`)

const start = async () => {
  try {
    const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017/portfolio'
    console.log(`trying to connect to ${MONGO_URL}`)
    await mongoose
      .connect(
        MONGO_URL,
        {
          useNewUrlParser: true,
          useCreateIndex: true,
        },
      )
    console.log('MongoDB connected.')

    await startExpress()
    console.log('done starting app.')
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}

start()
