const express = require('express')

const router = express.Router()
const auth = require('./middleware')
const {
  authenticate,
  createUser,
} = require('./service')

router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body
    const user = await authenticate(({ email, password }))
    if (!user) {
      res
        .status(401)
        .json({ reason: 'Login failed, Check credencials' })
        .end()
    }
    console.log(`${user.user.email} logged in`)
    res.json(user)
  } catch (error) {
    res
      .status(400)
      .json({ reason: error.message })
      .end()
  }
})

router.post('/', async (req, res) => {
  try {
    const { user, token } = await createUser(req.body)
    console.log(`${user.email} signed Up`)

    res
      .status(201)
      .json({ user, token })
      .end()
  } catch (error) {
    res
      .status(400)
      .json({ reason: error.message })
      .end()
  }
})


router.get('/me', auth, async (req, res) => {
  res.json(req.user)
})

router.post('/me/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => token.token !== req.token)
    await req.user.save()
    console.log(`${req.user.email} logged out`)
    res.json({ status: 'logged out' })
  } catch (error) {
    res.status(500).send(error)
  }
})

router.post('/me/logoutall', auth, async (req, res) => {
  try {
    req.user.tokens.splice(0, req.user.tokens.length)
    await req.user.save()
    console.log(`${req.user.email} logged out all`)
    res.json({ status: 'logged out all' })
  } catch (error) {
    res.status(500).send(error)
  }
})

module.exports = router
