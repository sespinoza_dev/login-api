const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')
const validator = require('validator')

const SECRET = process.env.SECRET || 'cupcake'

const userSchema = new mongoose.Schema({
  name: String,
  surname: String,
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: (value) => {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid Email address')
      }
    },
  },
  phone: String,
  webpage: String,
  password: {
    type: String,
    required: true,
    minLength: 7,
  },
  role: String,
  tokens: [{
    token: {
      type: String,
      required: true,
    },
  }],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
})

userSchema.pre('save', async function (next) {
  const user = this
  this.role = 'reader'
  if (user.isModified('password')) {
    user.password = await bcryptjs.hash(user.password, 8)
  }
  next()
})

userSchema.methods.generateAuthToken = async function () {
  const user = this
  console.log('user', user)
  const token = jwt.sign({ _id: user._id }, SECRET)
  user.tokens = user.tokens.concat({ token })
  await user.save()
  return token
}

userSchema.statics.findByCredentials = async function (email, password) {
  const user = await User.findOne({ email })
  if (!user) {
    throw new Error('Invalid login credentials')
  }
  const isPasswordMatch = await bcryptjs.compare(password, user.password)
  if (!isPasswordMatch) {
    throw new Error('Invalid login credentials')
  }
  return user
}

const User = mongoose.model('User', userSchema)

module.exports = User
