const User = require('./model')

exports.authenticate = async ({ email, password }) => {
  if (!email || !password) throw new Error('Email or password missing.')

  const user = await User.findByCredentials(email, password)
  if (!user) return null

  const token = await user.generateAuthToken()
  return ({ user, token })
}

exports.createUser = async (payload) => {
  const { email, password } = payload
  if (!email || !password) throw new Error('Email or password missing.')

  const findOne = await User.findOne({ email })

  if (findOne) throw new Error('Email already registered.')

  const user = new User(payload)
  await user.save()
  const token = await user.generateAuthToken()
  return ({ user, token })
}
