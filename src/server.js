const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { notFound } = require('./utils')

const BASE_URL = process.env.BASE_URL || '/login'
const router = require('./router')

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(BASE_URL, router)
app.get('*', notFound)

const startExpress = async () => {
  const port = process.env.PORT || 4004
  await app.listen(port)
  console.log('Express server started, listening in on ', port)
}

module.exports = {
  app,
  startExpress,
}
