FROM node:10.15.3-alpine

LABEL author="S.Espinoza <espinoza.jimenez@gmail.com>"

# Dependencies
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh && \
    apk add curl curl-dev git && \
    apk add g++ gcc libgcc libstdc++ linux-headers make python

RUN npm install --quiet node-gyp -g --build-from-source

# Directory
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/
COPY index.js package.json package-lock.json /usr/src/app/
COPY src /usr/src/app/src
COPY entrypoint.sh /usr/src/app/

# Application
RUN npm install --production
EXPOSE 4004
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]

CMD ["node", "/usr/src/app/index.js"]
